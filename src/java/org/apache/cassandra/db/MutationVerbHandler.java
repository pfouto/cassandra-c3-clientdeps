/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.apache.cassandra.db;

import java.io.DataInputStream;
import java.io.IOException;
import java.net.InetAddress;
import java.nio.ByteBuffer;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.apache.cassandra.db.partitions.PartitionUpdate;
import org.apache.cassandra.db.rows.BTreeRow;
import org.apache.cassandra.db.rows.Row;
import org.apache.cassandra.exceptions.WriteTimeoutException;
import org.apache.cassandra.io.util.FastByteArrayInputStream;
import org.apache.cassandra.net.*;
import org.apache.cassandra.schema.TableId;
import org.apache.cassandra.tracing.Tracing;
import org.apache.cassandra.utils.FBUtilities;
import pfouto.InternalConnection;
import pfouto.messaging.AckMessage;
import pfouto.messaging.Message;

public class MutationVerbHandler implements IVerbHandler<Mutation>
{

    private static final Logger logger = LoggerFactory.getLogger(MutationVerbHandler.class);
    private static final Logger visibilityLogger = LoggerFactory.getLogger("visibility");


    private void reply(int id, InetAddress replyTo, MessageIn<Mutation> message)
    {
        Tracing.trace("Enqueuing response to {}", replyTo);

        long now = System.currentTimeMillis();

        MessagingService.instance().sendReply(WriteResponse.createMessage(), id, replyTo);

        try
        {
            InternalConnection.getConnectionToInternal().writeAndFlush(
            new AckMessage(id, FBUtilities.getBroadcastAddress(), replyTo, System.currentTimeMillis()));
        }
        catch (IOException | InterruptedException e)
        {
            logger.error("Failled to send ack back to internal: " + id + ' ' + replyTo + ' ' + e.getMessage());
        }

        //pfouto s
        byte[] byteArray = message.parameters.get("T");
        if(byteArray!=null){
            ByteBuffer wrapped = ByteBuffer.wrap(byteArray); // big-endian by default
            int mutId = wrapped.getInt(); // 1
            long mTs = wrapped.getLong();
            long elapsed = now - mTs;
            visibilityLogger.info(replyTo + ":" + mutId + ':' + elapsed);
        }
        //pfouto e
    }

    private void failed()
    {
        Tracing.trace("Payload application resulted in WriteTimeout, not replying");
    }

    public void doVerb(MessageIn<Mutation> message, int id)  throws IOException
    {
        // Check if there were any forwarding headers in this message
        byte[] from = message.parameters.get(Mutation.FORWARD_FROM);
        InetAddress replyTo;
        if (from == null)
        {
            replyTo = message.from;
            //pfouto s comment
            //byte[] forwardBytes = message.parameters.get(Mutation.FORWARD_TO);
            //if (forwardBytes != null)
            //    forwardToLocalNodes(message.payload, message.verb, forwardBytes, message.from);
            //pfouto e
        }
        else
        {
            replyTo = InetAddress.getByAddress(from);
        }

        try
        {
            message.payload.applyFuture().thenAccept(o -> reply(id, replyTo, message)).exceptionally(wto -> {
                failed();
                return null;
            });
        }
        catch (WriteTimeoutException wto)
        {
            failed();
        }
    }

    public static void forwardToLocalNodes(Mutation mutation, MessagingService.Verb verb, byte[] forwardBytes, InetAddress from, byte[] ts) throws IOException
    {

        //Clone!
        Map<TableId, PartitionUpdate> newPus = new HashMap<>();
        for( PartitionUpdate pu : mutation.getPartitionUpdates()){
            List<Row> newRows = new LinkedList<>();
            for( Row row : pu){
                BTreeRow newRow = BTreeRow.create(row.clustering(), row.primaryKeyLivenessInfo(),
                                                  row.deletion(), ((BTreeRow) row).btree.clone());
                newRows.add(newRow);
            }

            PartitionUpdate newPu = new PartitionUpdate(pu.metadata(), pu.partitionKey(), pu.columns(), pu.rowCount());
            for(Row r : newRows){
                newPu.add(r);
            }
            newPus.put(newPu.metadata().id, newPu);
        }
        Mutation newMutation = new Mutation(mutation.getKeyspaceName(), mutation.key(), newPus);
        newMutation.createdAt = mutation.createdAt;
        mutation = newMutation;

        try (DataInputStream in = new DataInputStream(new FastByteArrayInputStream(forwardBytes)))
        {
            int size = in.readInt();

            // tell the recipients who to send their ack to
            MessageOut<Mutation> message = new MessageOut<>(verb, mutation, Mutation.serializer).withParameter(Mutation.FORWARD_FROM, from.getAddress());
            if(ts != null){
                message = message.withParameter("T", ts);
            }
            // Send a message to each of the addresses on our Forward List
            for (int i = 0; i < size; i++)
            {
                InetAddress address = CompactEndpointSerializationHelper.deserialize(in);
                int id = in.readInt();
                Tracing.trace("Enqueuing forwarded write to {}", address);
                MessagingService.instance().sendOneWay(message, id, address);
            }
        }
    }
}
