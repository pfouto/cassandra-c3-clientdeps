/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package pfouto;

import java.io.IOException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundHandlerAdapter;
import pfouto.messaging.DownMessage;

public class InternalHandler extends ChannelInboundHandlerAdapter
{

    private static final Logger logger = LoggerFactory.getLogger(InternalHandler.class);

    @Override
    public void channelActive(final ChannelHandlerContext ctx) { // (1)
        logger.info("New Channel Active:" + ctx.channel().remoteAddress());
    }

    @Override
    public void channelRead(ChannelHandlerContext ctx, Object msg)
    {
        DownMessage m = (DownMessage) msg;
        MessageConsumer.receiveFromInternal(m);
    }

    @Override
    public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) {
        logger.error("Error in Internal Handler: " + cause.getMessage() + ' ' + cause.getStackTrace()[0].getClassName() + ' ' + cause.getStackTrace()[1].getLineNumber());
        System.exit(1);
        ctx.close();
    }
}
