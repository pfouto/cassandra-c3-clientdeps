/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package pfouto;

import java.io.IOException;
import java.net.InetAddress;
import java.nio.ByteBuffer;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.apache.cassandra.config.DatabaseDescriptor;
import org.apache.cassandra.db.Mutation;
import org.apache.cassandra.db.MutationVerbHandler;
import org.apache.cassandra.db.ReadResponse;
import org.apache.cassandra.db.WriteResponse;
import org.apache.cassandra.db.marshal.Int32Type;
import org.apache.cassandra.db.marshal.UTF8Type;
import org.apache.cassandra.db.partitions.PartitionUpdate;
import org.apache.cassandra.db.rows.BTreeRow;
import org.apache.cassandra.db.rows.BufferCell;
import org.apache.cassandra.db.rows.CellPath;
import org.apache.cassandra.db.rows.ColumnData;
import org.apache.cassandra.db.rows.ComplexColumnData;
import org.apache.cassandra.db.rows.Row;
import org.apache.cassandra.net.MessageIn;
import org.apache.cassandra.net.MessagingService;
import org.apache.cassandra.schema.TableId;
import org.apache.cassandra.serializers.Int32Serializer;
import org.apache.cassandra.serializers.MapSerializer;
import org.apache.cassandra.serializers.UTF8Serializer;
import org.apache.cassandra.service.StorageProxy;
import org.apache.cassandra.utils.FBUtilities;
import org.apache.cassandra.utils.Pair;
import pfouto.messaging.DownMessage;
import pfouto.messaging.Message;

public class MessageConsumer
{
    private static final Logger logger = LoggerFactory.getLogger(MessageConsumer.class);

    //                               Node,        ID     Message
    private static final Map<Pair<InetAddress, Integer>, Object> waitingMap = new HashMap<>();

    public static void init()
    {

    }

    public static void receiveFromPeer(MessageIn<?> message, int id)
    {
        if (message.verb != MessagingService.Verb.MUTATION || message.parameters.get("T") == null)
        {
            logger.debug("Msg: " + id + " from " + message.from + " skipped metadata: " + message);
            MessagingService.instance().receive(message, id);
            return;
        }

        InetAddress sender = message.from;
        //Forward
        try
        {
            byte[] from = message.parameters.get(Mutation.FORWARD_FROM);
            if (from == null)
            {
                byte[] forwardBytes = message.parameters.get(Mutation.FORWARD_TO);
                if (forwardBytes != null)
                    MutationVerbHandler.forwardToLocalNodes((Mutation) message.payload, message.verb, forwardBytes, message.from, message.parameters.get("T"));
            }
            else
            {
                sender = InetAddress.getByAddress(from);
            }
        }
        catch (IOException e)
        {
            logger.error("Error forwarding message, discarding: " + e.getMessage());
            return;
        }

        Pair<InetAddress, Integer> p = Pair.create(sender, id);
        Object o = waitingMap.putIfAbsent(p, message);
        if (o != null)
        {
            waitingMap.remove(p);
            assert o instanceof DownMessage;
            DownMessage meta = (DownMessage) o;
            setMutationClock(message, meta.getOpClock());
            logger.debug("Executing received message: " + p);
            MessagingService.instance().receive(message, id);
        }
        else
        {
            logger.debug("Stored received message: " + p);
        }
    }

    static void receiveFromInternal(DownMessage m)
    {

        if (m.getOpId() != -1)
        {
            assert m.getFrom().equals(FBUtilities.getBroadcastAddress());
            StorageProxy.opClocks.get(m.getOpId()).complete(m.getOpClock());
            if (m.getMessageId() == -1) return;
        }

        assert m.getMessageId() != -1;

        Pair<InetAddress, Integer> p = Pair.create(m.getFrom(), m.getMessageId());
        Object o = waitingMap.putIfAbsent(p, m);
        if (o != null)
        {
            waitingMap.remove(p);
            assert o instanceof MessageIn<?>;
            MessageIn<?> mutation = (MessageIn<?>) o;
            setMutationClock(mutation, m.getOpClock());
            logger.debug("Executing received metadata: " + p);
            MessagingService.instance().receive(mutation, m.getMessageId());
        }
        else
        {
            logger.debug("Stored received metadata: " + p);
        }
    }

    private static void setMutationClock(MessageIn<?> message, Map<String, Integer> opClock)
    {
        assert message.payload instanceof Mutation;

        Mutation mutation = (Mutation) message.payload;

        //pfouto s

        //assume single partition update with single row

        Row r = mutation.getPartitionUpdates().iterator().next().iterator().next();
        Iterator<ColumnData> clock = r.stream().filter(cd -> cd.column().name.toString().equals("clock")).iterator();

        if(!clock.hasNext()){
            logger.warn("Message without clock! (probably system message or something): " + message);
            return;
        }

        ComplexColumnData c = (ComplexColumnData) clock.next();


        ComplexColumnData.Builder builder = ComplexColumnData.builder();
        builder.newColumn(c.column());
        for(Map.Entry<String, Integer> clockEntry : opClock.entrySet()){
            CellPath cellPath = CellPath.create(UTF8Serializer.instance.serialize(clockEntry.getKey()));
            BufferCell live = BufferCell.live(c.column(), c.iterator().next().timestamp(),
                                              Int32Serializer.instance.serialize(clockEntry.getValue()), cellPath);
            builder.addCell(live);
        }
        ComplexColumnData build = builder.build();
        Object[] btree = ((BTreeRow) r).btree;
        boolean done = false;
        for(int i = 0;i<btree.length;i++){
            if(btree[i] == c){
                btree[i] = build;
                done = true;
                break;
            }
        }
        assert done;
    }


    private static long nMetadata(Collection<Object> values)
    {
        return values.stream().filter(o -> (o instanceof DownMessage)).count();
    }

    private static long nData(Collection<Object> values)
    {
        return values.stream().filter(o -> (o instanceof MessageIn<?>)).count();
    }
}
