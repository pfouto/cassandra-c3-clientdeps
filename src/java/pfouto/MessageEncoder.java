/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package pfouto;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import io.netty.buffer.ByteBuf;
import io.netty.buffer.ByteBufOutputStream;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelOutboundHandlerAdapter;
import io.netty.channel.ChannelPromise;
import pfouto.messaging.AckMessage;
import pfouto.messaging.Message;
import pfouto.messaging.UpMessage;

public class MessageEncoder extends ChannelOutboundHandlerAdapter
{
    private static final Logger logger = LoggerFactory.getLogger(MessageEncoder.class);

    @Override
    public void write(ChannelHandlerContext ctx, Object o, ChannelPromise promise)
    {
        if (o instanceof UpMessage || o instanceof AckMessage)
        {
            ByteBuf out = ctx.alloc().buffer();
            try
            {
                ByteBufOutputStream outStream = new ByteBufOutputStream(out);
                //Reserve space for message size
                out.writerIndex(4);
                //Encode message
                Message m = (Message) o;
                m.encode(outStream);
                //Update message size
                int index = out.writerIndex();
                out.writerIndex(0);
                out.writeInt(index - 4);
                out.writerIndex(index);
                ctx.write(out, promise);
            }
            catch (Exception e)
            {
                if (out != null && out.refCnt() > 0) out.release(out.refCnt());
                promise.tryFailure(e);
                logger.error("Exception encoding message: " + e.getMessage() + ' '
                             + e.getStackTrace()[0].getClassName() + ' '
                             + e.getStackTrace()[0].getLineNumber());
                System.exit(1);
            }
        }
        else
        {
            logger.error("Unsupported object received in encoder: " + o);
        }
    }
}