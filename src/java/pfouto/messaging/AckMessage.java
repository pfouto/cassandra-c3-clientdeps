/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package pfouto.messaging;

import java.io.IOException;
import java.net.InetAddress;

import io.netty.buffer.ByteBufInputStream;
import io.netty.buffer.ByteBufOutputStream;
import org.apache.cassandra.net.CompactEndpointSerializationHelper;

public class AckMessage extends Message{

    public final static int CODE = 4;

    private final int id;
    private final InetAddress node;

    public AckMessage(int messageId, InetAddress node, InetAddress from, long timestamp) {
        super(from, CODE, timestamp);
        this.id = messageId;
        this.node = node;
    }

    public int getId() {
        return id;
    }

    public InetAddress getNode() {
        return node;
    }

    @Override
    public String toString(){
        return super.toString() + " ID: " + id + " NODE: " + node;
    }

    @Override
    protected void encodeSpecificParameters(ByteBufOutputStream outStream) throws IOException
    {
        outStream.writeInt(id);
        CompactEndpointSerializationHelper.serialize(node, outStream);
    }

    static AckMessage specificDecodeMessage(ByteBufInputStream inputStream, InetAddress from, long timestamp) throws IOException {
        int id = inputStream.readInt();
        InetAddress node = CompactEndpointSerializationHelper.deserialize(inputStream);
        return new AckMessage(id, node, from, timestamp);
    }

}