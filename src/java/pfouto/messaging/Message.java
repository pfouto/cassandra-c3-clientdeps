/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package pfouto.messaging;

import java.io.IOException;
import java.net.InetAddress;

import io.netty.buffer.ByteBufInputStream;
import io.netty.buffer.ByteBufOutputStream;
import org.apache.cassandra.net.CompactEndpointSerializationHelper;

public abstract class Message
{
    private final InetAddress from;
    private final int code;
    private final long timestamp;

    Message(InetAddress from, int code, long timestamp)
    {
        this.from = from;
        this.code = code;
        this.timestamp = timestamp;
    }

    public InetAddress getFrom()
    {
        return from;
    }

    public int getCode()
    {
        return code;
    }

    public long getTimestamp()
    {
        return timestamp;
    }

    @Override
    public String toString()
    {
        return "CODE: " + code + " FROM: " + from;
    }

    public void encode(ByteBufOutputStream outStream) throws IOException
    {
        outStream.writeInt(code);
        CompactEndpointSerializationHelper.serialize(from, outStream);
        outStream.writeLong(timestamp);
        encodeSpecificParameters(outStream);
    }

    protected abstract void encodeSpecificParameters(ByteBufOutputStream outStream) throws IOException;

    public static Message decodeMessage(ByteBufInputStream inputStream) throws Exception {
        int messageCode = inputStream.readInt();
        InetAddress from = CompactEndpointSerializationHelper.deserialize(inputStream);
        long timestamp = inputStream.readLong();

        switch (messageCode){
            case AckMessage.CODE:
                return AckMessage.specificDecodeMessage(inputStream, from, timestamp);
            case DownMessage.CODE:
                return DownMessage.specificDecodeMessage(inputStream, from, timestamp);
            case UpMessage.CODE:
                return UpMessage.specificDecodeMessage(inputStream, from, timestamp);
            default:
                throw new Exception("unknown/unhandled messageType in decoding: " + messageCode);
        }
    }
}
